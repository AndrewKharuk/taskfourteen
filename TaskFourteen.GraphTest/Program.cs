﻿using System;
using TaskFour.GraphWorker;

namespace TaskFourteen.GraphTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var g = new Graph<GraphNode<GraphEdge>, GraphEdge>();

            g.AddNode(1, "A");
            g.AddNode(2, "B");
            g.AddNode(3, "C");
            g.AddNode(4, "D");
            g.AddNode(5, "E");
            g.AddNode(6, "F");
            g.AddNode(7, "G");

            g.AddEdge(1, 2, 22);
            g.AddEdge(1, 3, 33);
            g.AddEdge(1, 4, 61);
            g.AddEdge(2, 3, 47);
            g.AddEdge(2, 5, 93);
            g.AddEdge(3, 4, 11);
            g.AddEdge(3, 5, 79);
            g.AddEdge(3, 6, 63);
            g.AddEdge(4, 6, 41);
            g.AddEdge(5, 6, 17);
            g.AddEdge(5, 7, 58);
            g.AddEdge(6, 7, 84);

            var dijkstra = new Dijkstra<GraphNode<GraphEdge>, GraphEdge>(g);
            var path = dijkstra.FindShortestPathByNodeName("A", "G");
            Console.WriteLine(path);

            path = g.GetAllNode();
            Console.WriteLine(path);

            try
            {
                g.RemoveNode(2);
                g.RemoveNode(4);
                g.RemoveNode(6);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            path = g.GetAllNode();
            Console.WriteLine(path);

            Console.ReadLine();
        }
    }
}
