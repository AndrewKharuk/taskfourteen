﻿using System.Collections.Generic;

namespace TaskFour.GraphWorker
{
    /// <summary>
    /// Вершина графа
    /// </summary>
    public class GraphNode<TEdge> : INode<TEdge> where TEdge : class, IEdge, new()
    {
        /// <summary>
        /// Идентификатор вершины
        /// </summary>
        public int NodeId { get; set; }

        /// <summary>
        /// Название вершины
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Список ребер
        /// </summary>
        public List<TEdge> Edges { get; set; }

        /// <summary>
        /// Добавить ребро
        /// </summary>
        /// <param name="newEdge">Ребро</param>
        public void AddEdge(TEdge newEdge)
        {
            Edges.Add(newEdge);
        }

        /// <summary>
        /// Добавить ребро
        /// </summary>
        /// <param name="nodeId">Идентификатор вершины</param>
        /// <param name="edgeWeight">Вес</param>
        public void AddEdge(int nodeId, int edgeWeight)
        {
            TEdge t = new TEdge();
            t.ConnectedNodeId = nodeId;
            t.EdgeWeight = edgeWeight;

            AddEdge(t);
        }

        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns>Имя вершины</returns>
        public override string ToString() => $"{NodeId}:{Name}";
    }
}
