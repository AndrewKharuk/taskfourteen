﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TaskFour.GraphWorker
{
    /// <summary>
    /// Граф
    /// </summary>
    public class Graph<TNode, TEdge>  where TNode : class, INode<TEdge>, new() 
                                      where TEdge : class, IEdge, new()
    {
        /// <summary>
        /// Список вершин графа
        /// </summary>
        public List<TNode> Nodes { get; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public Graph()
        {
            Nodes = new List<TNode>();
        }

        public string GetAllNode()
        {
            return string.Join(" || ", Nodes.Select(n => $"{n.NodeId}:{n.Name}"));
        }

        /// <summary>
        /// Добавление вершины
        /// </summary>
        /// <param name="nodeName">Имя вершины</param>
        public void AddNode(int id, string nodeName)
        {
            TNode t = new TNode();
            t.NodeId = id;
            t.Name = nodeName;
            t.Edges = new List<TEdge>();
            Nodes.Add(t);
        }

        /// <summary>
        /// Проверяет наличие вершины по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор вершины</param>
        /// <returns>Результат</returns>
        public bool ContainsNodeById(int id)
        {
            return Nodes.Exists(e => e.NodeId == id);
        }

        /// <summary>
        /// Проверяет наличие вершины по имени
        /// </summary>
        /// <param name="nodeName">Имя вершины</param>
        /// <returns>Результат</returns>
        public bool ContainsNodeByName(string nodeName)
        {
            return Nodes.Exists(e => e.Name == nodeName);
        }

        /// <summary>
        /// Удаление вершины по идентификатору или имени
        /// </summary>
        /// <param name="nodeId">Идентификатор вершины</param>
        /// <param name="nodeName">Имя вершины</param>
        public void RemoveNode(int? nodeId = null, string nodeName = null )
        {
            TNode node = default(TNode);
            if (nodeId != null && ContainsNodeById(nodeId.Value))
                node = FindNodeById(nodeId.Value);
            else if (nodeId == null && nodeName != null && ContainsNodeByName(nodeName))
                node = FindNodeByName(nodeName);

            if (node == null)
                throw new Exception("Node not specified");

            RemoveNode(node);
        }

        /// <summary>
        /// Удаление вершины
        /// </summary>
        /// <param name="node">Вершина</param>
        public void RemoveNode(TNode node)
        {
            foreach (var edge in node.Edges)
            {
                TNode connectedNode = FindNodeById(edge.ConnectedNodeId);
                TEdge connectedEdge = default(TEdge);

                if (connectedNode != null)
                    connectedEdge = connectedNode.Edges.FirstOrDefault(e => e.ConnectedNodeId == node.NodeId);

                if(connectedEdge != null)
                    RemoveEdge(connectedNode, connectedEdge);
            }  
            
            Nodes.Remove(node);
        }

        /// <summary>
        /// Поиск вершины по названию
        /// </summary>
        /// <param name="nodeName">Название вершины</param>
        /// <returns>Найденная вершина</returns>
        public TNode FindNodeByName(string nodeName)
        {
            foreach (var v in Nodes)
            {
                if (v.Name.Equals(nodeName))
                {
                    return v;
                }
            }

            return null;
        }

        /// <summary>
        /// Поиск вершины по идентификатору
        /// </summary>
        /// <param name="nodeId">Идентификатор вершины</param>
        /// <returns>Найденная вершина</returns>
        public TNode FindNodeById(int nodeId)
        {
            foreach (var v in Nodes)
            {
                if (v.NodeId == nodeId)
                {
                    return v;
                }
            }

            return null;
        }


        /// <summary>
        /// Добавление ребра
        /// </summary>
        /// <param name="firstNodeId">Индекс первой вершины</param>
        /// <param name="secondNodeId">Индекс второй вершины</param>
        /// <param name="weight">Вес ребра</param>
        public void AddEdge(int firstNodeId, int secondNodeId, int weight)
        {
            TNode v1 = FindNodeById(firstNodeId);
            TNode v2 = FindNodeById(secondNodeId);
            if (v2 != null && v1 != null)
            {
                v1.AddEdge(v2.NodeId, weight);
                v2.AddEdge(v1.NodeId, weight);
            }
        }

        /// <summary>
        /// Удаление ребра
        /// </summary>
        /// <param name="node">Вершина</param>
        /// <param name="edge">Ребро</param>
        public void RemoveEdge(TNode node, TEdge edge)
        {
            node.Edges.Remove(edge);
        }
    }
}
