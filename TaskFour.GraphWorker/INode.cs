﻿using System.Collections.Generic;

namespace TaskFour.GraphWorker
{
    public interface INode<TEdge> where TEdge : IEdge, new() 
    {
        /// <summary>
        /// Идентификатор вершины
        /// </summary>
        int NodeId { get; set; }

        /// <summary>
        /// Название вершины
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Список ребер
        /// </summary>
        List<TEdge> Edges { get; set; }

        /// <summary>
        /// Добавить ребро
        /// </summary>
        /// <param name="node">Идентификатор вершины</param>
        /// <param name="edgeWeight">Вес</param>
        void AddEdge(int nodeId, int edgeWeight);
    }
}
