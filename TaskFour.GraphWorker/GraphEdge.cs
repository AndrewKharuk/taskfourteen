﻿namespace TaskFour.GraphWorker
{
    /// <summary>
    /// Ребро графа
    /// </summary>
    public class GraphEdge : IEdge
    {
        /// <summary>
        /// Связанная вершина
        /// </summary>
        public int ConnectedNodeId { get; set; }

        /// <summary>
        /// Вес ребра
        /// </summary>
        public int EdgeWeight { get; set; }
    }
}
