﻿using System.Collections.Generic;
using System.Linq;

namespace TaskFour.GraphWorker
{
    /// <summary>
    /// Алгоритм Дейкстры
    /// </summary>
    public class Dijkstra<TNode, TEdge> where TNode : class, INode<TEdge>, new()
                                        where TEdge : class, IEdge, new()
    {
        Graph<TNode, TEdge> graph;

        List<GraphNodeInfo<TNode, TEdge>> infos;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="graph">Граф</param>
        public Dijkstra(Graph<TNode, TEdge> graph)
        {
            this.graph = graph;
        }

        /// <summary>
        /// Инициализация информации
        /// </summary>
        void InitInfo()
        {
            infos = new List<GraphNodeInfo<TNode, TEdge>>();
            foreach (var v in graph.Nodes)
            {
                infos.Add(new GraphNodeInfo<TNode, TEdge>(v));
            }
        }

        /// <summary>
        /// Получение информации о вершине графа
        /// </summary>
        /// <param name="v">Вершина</param>
        /// <returns>Информация о вершине</returns>
        GraphNodeInfo<TNode, TEdge> GetNodeInfo(TNode v)
        {
            foreach (var i in infos)
            {
                if (i.Node.Equals(v))
                {
                    return i;
                }
            }

            return null;
        }

        /// <summary>
        /// Поиск непосещенной вершины с минимальным значением суммы
        /// </summary>
        /// <returns>Информация о вершине</returns>
        public GraphNodeInfo<TNode, TEdge> FindUnvisitedNodeWithMinSum()
        {
            var minValue = int.MaxValue;
            GraphNodeInfo<TNode, TEdge> minNodeInfo = null;
            foreach (var i in infos)
            {
                if (i.IsUnvisited && i.EdgesWeightSum < minValue)
                {
                    minNodeInfo = i;
                    minValue = i.EdgesWeightSum;
                }
            }

            return minNodeInfo;
        }

        /// <summary>
        /// Поиск кратчайшего пути по названиям вершин
        /// </summary>
        /// <param name="startName">Название стартовой вершины</param>
        /// <param name="finishName">Название финишной вершины</param>
        /// <returns>Кратчайший путь</returns>
        public string FindShortestPathByNodeName(string startName, string finishName)
        {
            return FindShortestPath(graph.FindNodeByName(startName), graph.FindNodeByName(finishName));
        }

        /// <summary>
        /// Поиск кратчайшего пути по вершинам
        /// </summary>
        /// <param name="startNode">Стартовая вершина</param>
        /// <param name="finishNode">Финишная вершина</param>
        /// <returns>Кратчайший путь</returns>
        public string FindShortestPath(TNode startNode, TNode finishNode)
        {
            InitInfo();
            var first = GetNodeInfo(startNode);
            first.EdgesWeightSum = 0;
            while (true)
            {
                var current = FindUnvisitedNodeWithMinSum();
                if (current == null)
                {
                    break;
                }

                SetSumToNextVertex(current);
            }

            return GetPath(startNode, finishNode);
        }

        /// <summary>
        /// Вычисление суммы весов ребер для следующей вершины
        /// </summary>
        /// <param name="info">Информация о текущей вершине</param>
        void SetSumToNextVertex(GraphNodeInfo<TNode, TEdge> info)
        {
            info.IsUnvisited = false;
            foreach (var e in info.Node.Edges)
            {
                var node = graph.FindNodeById(e.ConnectedNodeId);
                var nextInfo = GetNodeInfo(node);
                var sum = info.EdgesWeightSum + e.EdgeWeight;
                if (sum < nextInfo.EdgesWeightSum)
                {
                    nextInfo.EdgesWeightSum = sum;
                    nextInfo.PreviousNode = info.Node;
                }
            }
        }

        /// <summary>
        /// Формирование пути
        /// </summary>
        /// <param name="startNode">Начальная вершина</param>
        /// <param name="endNode">Конечная вершина</param>
        /// <returns>Путь</returns>
        string GetPath(TNode startNode, TNode endNode)
        {
            int allPathWeigh = 0;

            var path = endNode.ToString();
            while (startNode != endNode)
            {
                //Get Edge weight
                int edgesWeigh = GetEdgesWeight(GetNodeInfo(endNode).PreviousNode.NodeId, endNode.NodeId);
                allPathWeigh += edgesWeigh;

                endNode = GetNodeInfo(endNode).PreviousNode;
                path = $" => {endNode.ToString()} _weight:{edgesWeigh}_ {path}";
            }

            return $"{path}\r\nAll weigh: {allPathWeigh}";
        }

        /// <summary>
        /// Получение веса ребра
        /// </summary>
        /// <param name="firstNodeId">Начальная вершина</param>
        /// <param name="secondNodeId">Конечная вершина</param>
        /// <returns>Вес ребра</returns>
        int GetEdgesWeight(int firstNodeId, int secondNodeId)
        {
            TNode firstNode = graph.FindNodeById(firstNodeId);
            int edgesWeight = firstNode.Edges.FirstOrDefault(e => e.ConnectedNodeId == secondNodeId).EdgeWeight;

            return edgesWeight;
        }
    }
}
