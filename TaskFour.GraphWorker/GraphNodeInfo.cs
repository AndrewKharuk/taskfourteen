﻿namespace TaskFour.GraphWorker
{
    /// <summary>
    /// Информация о вершине
    /// </summary>
    public class GraphNodeInfo<TNode, TEdge> where TNode : class, INode<TEdge>, new()
                                             where TEdge : class, IEdge, new()
    {
        /// <summary>
        /// Вершина
        /// </summary>
        public TNode Node { get; set; }

        /// <summary>
        /// Не посещенная вершина
        /// </summary>
        public bool IsUnvisited { get; set; }

        /// <summary>
        /// Сумма весов ребер
        /// </summary>
        public int EdgesWeightSum { get; set; }

        /// <summary>
        /// Предыдущая вершина
        /// </summary>
        public TNode PreviousNode { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="node">Вершина</param>
        public GraphNodeInfo(TNode node)
        {
            Node = node;
            IsUnvisited = true;
            EdgesWeightSum = int.MaxValue;
            PreviousNode = null;
        }
    }
}
