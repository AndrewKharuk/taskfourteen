﻿namespace TaskFour.GraphWorker
{
    public interface IEdge
    {
        /// <summary>
        /// Связанная вершина
        /// </summary>
        int ConnectedNodeId { get; set; }

        /// <summary>
        /// Вес ребра
        /// </summary>
        int EdgeWeight { get; set; }
    }
}
